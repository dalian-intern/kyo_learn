from tkinter import *
from tkinter import ttk
import tkinter.messagebox as messagebox
import math

root=Tk()
root.title('電卓')
frame1 = ttk.Frame(root)

def foo1():
	u = []
	aa=int(a.get())
	bb=int(b.get())
	for num in range(aa,bb+1):
		if num>1:
			for i in range(2,num):
				if(num%i)==0:
					break
			else:
				u.append(num)	
	messagebox.showinfo("私は探した素数",u)				
	
	
a = StringVar()
b = StringVar()
label11 = ttk.Label(frame1, text='素数を探す:',foreground='#E53C05',padding=(5,10))
entry11 = ttk.Entry(frame1, textvariable=a)
label12 = ttk.Label(frame1, text='～')
entry12 = ttk.Entry(frame1, textvariable=b)
button11 = ttk.Button(frame1, text='計算',command=foo1)



frame1.grid()
label11.grid()
entry11.grid()
label12.grid()
entry12.grid()
button11.grid()


def foo2():
	cc=int(c.get())
	dd=int(d.get())
	ee=int(e.get())
	ff=int(f.get())
	min=cc
	max=cc
	u=[]
	u.append(cc)
	u.append(dd)
	u.append(ee)
	u.append(ff)
	for i in range(1,4):
		if u[i]>max:
			max=u[i]
		elif u[i]<min:
			min=u[i]
	fff=("max:",max,"min:",min)
	messagebox.showinfo("大きさと小さい",fff)
c = StringVar()
d = StringVar()
e = StringVar()
f = StringVar()
label21 = ttk.Label(frame1, text='最大値と最小値:',foreground='#E53C05',padding=(5,10))
entry21 = ttk.Entry(frame1, textvariable=c,)
entry22 = ttk.Entry(frame1, textvariable=d,)
entry23 = ttk.Entry(frame1, textvariable=e,)
entry24 = ttk.Entry(frame1, textvariable=f,)
button21 = ttk.Button(frame1, text='計算',command=foo2)
label21.grid()
entry21.grid()
entry22.grid()
entry23.grid()
entry24.grid()
button21.grid()


def is_true(n):
	s=str(n)
	return s[:]==s[::-1]
def foo3():
	u=[]
	gg=int(g.get())
	hh=int(h.get())
	for i in filter(is_true,range(gg,hh)):
		u.append(i)
	messagebox.showinfo("回数を探す",u)
g = StringVar()
h = StringVar()
label31 = ttk.Label(frame1, text='回文数を探す:',foreground='#E53C05',padding=(5,10))
entry31 = ttk.Entry(frame1, textvariable=g)
label32 = ttk.Label(frame1, text='～')
entry32 = ttk.Entry(frame1, textvariable=h)
button31 = ttk.Button(frame1,text='計算',command=foo3)
label31.grid(row=0,column=2)
entry31.grid(row=1,column=2)
label32.grid(row=2,column=2)
entry32.grid(row=3,column=2)
button31.grid(row=4,column=2)

def foo4():
	xx=int(x.get())
	yy=int(y.get())
	u=(xx*yy)//math.gcd(xx,yy)
	messagebox.showinfo("最小公倍数",u)

x = StringVar()
y = StringVar()
label41 = ttk.Label(frame1, text='最小公倍数:',foreground='#E53C05',padding=(5,10))
entry41 = ttk.Entry(frame1, textvariable=x)
entry42 = ttk.Entry(frame1, textvariable=y)
button41 = ttk.Button(frame1,text='計算',command=foo4)
label41.grid(row=5,column=2)
entry41.grid(row=6,column=2)
entry42.grid(row=7,column=2)
button41.grid(row=8,column=2)


root.mainloop()